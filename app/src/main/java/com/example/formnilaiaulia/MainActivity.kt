package com.example.formnilaiaulia

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var absenNilai : TextInputEditText
    private lateinit var tugasNilai : TextInputEditText
    private lateinit var utsNilai : TextInputEditText
    private lateinit var uasNilai : TextInputEditText
    private lateinit var submitButton : Button
    private lateinit var outputHasil : TextView
    private lateinit var outputGrade : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        absenNilai = findViewById(R.id.editHadir)
        tugasNilai = findViewById(R.id.editTugas)
        utsNilai = findViewById(R.id.inputUTS)
        uasNilai = findViewById(R.id.inputUAS)

        submitButton = findViewById(R.id.btnSubmit)
        outputHasil = findViewById(R.id.hasilOutput)
        outputGrade = findViewById(R.id.gradeOutput)

        submitButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnSubmit -> {
                var isEmptyField = false
                val nilaiAbsen = absenNilai.text.toString().trim()
                val nilaiTugas = tugasNilai.text.toString().trim()
                val nilaiUts = utsNilai.text.toString().trim()
                val nilaiUas = uasNilai.text.toString().trim()

                if (nilaiAbsen.isEmpty()){
                    isEmptyField = true
                    absenNilai.error = "Inputlah nilai absen"
                }else if (nilaiAbsen.toInt() > 100 || nilaiAbsen.toInt() < 0){
                    isEmptyField = true
                    absenNilai.error = "kisaran angka harus diatas 0 hingga 100"
                }

                if (nilaiTugas.isEmpty()){
                    isEmptyField = true
                    tugasNilai.error = "Inputlah nilai tugas"
                }else if (nilaiTugas.toInt() > 100 || nilaiTugas.toInt() < 0){
                    isEmptyField = true
                    absenNilai.error = "kisaran angka harus diatas 0 hingga 100"
                }

                if (nilaiUts.isEmpty()){
                    isEmptyField = true
                    utsNilai.error = "Inputlah nilai UTS"
                }else if (nilaiUts.toInt() > 100 || nilaiUts.toInt() < 0){
                    isEmptyField = true
                    absenNilai.error = "kisaran angka harus diatas 0 hingga 100"
                }

                if (nilaiUas.isEmpty()){
                    isEmptyField = true
                    uasNilai.error = "Inputlah nilai UAS"
                }else if (nilaiUas.toInt() > 100 || nilaiUas.toInt() < 0){
                    isEmptyField = true
                    absenNilai.error = "kisaran angka harus diatas 0 hingga 100"
                }

                if (!isEmptyField) {
                    val hitung = HitungNilai(
                        nilaiAbsen.toInt(),
                        nilaiTugas.toInt(),
                        nilaiUts.toInt(),
                        nilaiUas.toInt()
                    )


                    outputHasil.text = "sebesar ${hitung.HitungNilai()}"
                    outputGrade.text = "huruf ${hitung.HitungGrade()}"
                }
            }
        }
    }
}

class HitungNilai(
    var absen : Int,
    var tugas : Int,
    var uts : Int,
    var uas : Int
) : getNilai() {

    var fixAbsen = super.getNilai(absen, 10.0)
    var fixTugas = super.getNilai(tugas, 20.0)
    var fixUts = super.getNilai(uts, 30.0)
    var fixUas = super.getNilai(uas, 40.0)

    fun HitungNilai(): Int {
        val hasil =  fixAbsen + fixTugas + fixUts + fixUas
        return hasil
    }
    fun HitungGrade(): String{
        val hasil = fixAbsen + fixTugas + fixUts + fixUas
        when(hasil){
            in 0..50 -> return "E"
            in 50..60 -> return "D"
            in 60..70 -> return "C"
            in 70..80 -> return "B"
            in 80..100 -> return "A"
            else -> return "null"
        }
    }
}

open class getNilai(){
    open fun getNilai(nilai: Int, percent: Double): Int {
        val hasil = (nilai * percent) / 100
        return hasil.toInt()
    }
}